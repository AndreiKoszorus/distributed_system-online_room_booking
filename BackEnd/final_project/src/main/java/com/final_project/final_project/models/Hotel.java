package com.final_project.final_project.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "hotels")
public class Hotel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String name;

    @OneToOne( targetEntity = Location.class)
    @JoinColumn(name = "location_id")
    private Location locationId;

    @Column
    private int stars;

    @Column
    private String rating;

    @Column
    private String facilities;

   @Column
   private String imageUrl;




    public Hotel(String name, Location locationId, int stars, String rating, String facilities,String imgUrl) {
        this.name = name;
        this.locationId = locationId;
        this.stars = stars;
        this.rating = rating;
        this.facilities = facilities;
        //this.owner = owner;
        this.imageUrl = imgUrl;
    }

    public Hotel(){

    }

    /*public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }*/

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocationId() {
        return locationId;
    }

    public void setLocationId(Location locationId) {
        this.locationId = locationId;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }


    public String toString(){
        return "Hotel: { "+
                ",name = "+getName()+
                ", location = "+getLocationId()+
                ", stars = "+getStars()+
                ", rating = "+getRating()+
                ",facilities = "+getFacilities()+
                " }";
    }
}
