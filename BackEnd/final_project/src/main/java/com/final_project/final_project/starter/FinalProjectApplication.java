package com.final_project.final_project.starter;

import com.final_project.final_project.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
@ComponentScan(basePackages = {"com.final_project.final_project.controller","com.final_project.final_project.service","com.final_project.final_project.repository"})
@EntityScan(basePackages = {"com.final_project.final_project.models"})
@EnableJpaRepositories(basePackages = {"com.final_project.final_project.repository"})
public class FinalProjectApplication {

	public static void main(String[] args) {


		SpringApplication.run(FinalProjectApplication.class, args);
	}

}
