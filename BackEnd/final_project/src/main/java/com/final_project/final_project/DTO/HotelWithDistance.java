package com.final_project.final_project.DTO;

import com.final_project.final_project.models.Hotel;

import java.io.Serializable;

public class HotelWithDistance implements Serializable {

    private Hotel hotel;
    private double distance;

    public HotelWithDistance(Hotel hotel, double distance) {
        this.hotel = hotel;
        this.distance = distance;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
