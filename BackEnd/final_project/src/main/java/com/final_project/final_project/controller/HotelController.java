package com.final_project.final_project.controller;


import com.final_project.final_project.DTO.HotelWithDistance;
import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.service.HotelService;
import com.final_project.final_project.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/hotels")
@CrossOrigin
public class HotelController {
    @Autowired
    private HotelService hotelService;
    @GetMapping(value = "/filter_by_stars/{stars}",produces = "application/json")
    public List<Hotel> filterByStars(@PathVariable("stars") int stars){
        return hotelService.findAllByStars(stars);
    }

    @GetMapping(value = "/filter_by_rating/{rating}",produces = "application/json")
    public List<Hotel> filterByRating(@PathVariable("rating") String rating){
        return hotelService.findAllByRating(rating);
    }

    @GetMapping(value = "/filter_by_rating_and_stars/{rating}/{stars}",produces = "application/json")
    public List<Hotel> filterByStarsAndRating(@PathVariable("rating") String rating,@PathVariable("stars") int stars){
        return hotelService.findAllByRatingAndStars(rating,stars);
    }

    @GetMapping(value = "/filter_by_distance/{lat1}/{lon1}/{distance}",produces = "application/json")
    public List<HotelWithDistance> calcDistance(@PathVariable("lon1") double lon1, @PathVariable("lat1") double lat1, @PathVariable("distance") String distance){
        double transformedDistance = Double.parseDouble(distance);
        return hotelService.filterByDistance(transformedDistance,lat1,lon1);
    }

    @PutMapping(value = "/hotel",produces = "application/json")
    public void putHotel(@RequestBody Hotel hotel){
        hotelService.saveHotel(hotel);
    }

    @DeleteMapping(value = "/{id}",produces = "application/json")
    public boolean deleteHotel(@PathVariable(name = "id") int id){
        return hotelService.deleteHotel(id);
    }

}
