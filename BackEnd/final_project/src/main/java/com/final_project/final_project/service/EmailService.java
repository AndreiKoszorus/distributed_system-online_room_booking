package com.final_project.final_project.service;


import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService {

public void sendEmail(String to,String subject,String content){
    Properties properties = new Properties();
    properties.put("mail.smtp.auth", "true");
    properties.put("mail.smtp.starttls.enable", "true");
    properties.put("mail.smtp.host", "smtp.gmail.com");
    properties.put("mail.smtp.port", "587");
    properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");

    Session session = Session.getInstance(properties,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("sd.proiect.3.2@gmail.com", "proiect_3");
                }
            });

    try {

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("sd.proiect.3.2@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);
        message.setText(content);

        Transport.send(message);

        System.out.println("Mail sent.");
    } catch (MessagingException e) {
        e.printStackTrace();
    }

}

}
