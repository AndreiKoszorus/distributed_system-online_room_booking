package com.final_project.final_project.models;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "reservations")
public class Reservation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "check_in")
    private Timestamp checkIn;

    @Column(name = "check_out")
    private Timestamp checkOut;

    @ManyToOne(targetEntity = Hotel.class)
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;


    @ManyToOne(targetEntity = Room.class)
    @JoinColumn(name = "room_id")
    private Room reservedRoom;

    @Column
    private String username;


    public Reservation(Timestamp checkIn, Timestamp checkOut, Hotel hotel, Room reservedRoom,String username) {
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.hotel = hotel;
        this.reservedRoom = reservedRoom;
        this.username = username;

    }
    public Reservation(){

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Timestamp checkIn) {
        this.checkIn = checkIn;
    }

    public Timestamp getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Timestamp checkOut) {
        this.checkOut = checkOut;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Room getReservedRoom() {
        return reservedRoom;
    }

    public void setReservedRoom(Room reservedRoom) {
        this.reservedRoom = reservedRoom;
    }

    public String toString(){
        return "Reservation: "+
                ",checkIn: "+getCheckIn().toString()+
                ",checkOut: "+getCheckOut().toString()+
                ",hotel: "+getHotel()+
                ",room: "+getReservedRoom()+
                " }";
    }
}
