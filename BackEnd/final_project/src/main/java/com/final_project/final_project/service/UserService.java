package com.final_project.final_project.service;

import com.final_project.final_project.models.User;
import com.final_project.final_project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


    @Service
    public class UserService {

        @Autowired
        UserRepository userRepository;

        public User findUser(String username){
            return userRepository.findByUsername(username);
        }

        public void saveUser(User user){
            userRepository.save(user);
        }

        public void deleteUser(User user){
            userRepository.delete(user);
        }

        public User findUserByUsernameAndPassword(String username,String password){
            return userRepository.findByUsernameAndAndPassword(username,password);
        }
    }
