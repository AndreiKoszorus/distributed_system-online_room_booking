package com.final_project.final_project.repository;

import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface RoomRepository extends JpaRepository<Room,Integer> {

     List<Room> findAllByPrice(double price);
     List<Room> findAllByNumberOfPersons(int numberOfPersons);
     Room findById(int id);
     List<Room> findAllByHotel(Hotel hotel);
}
