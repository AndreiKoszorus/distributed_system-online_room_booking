package com.final_project.final_project.controller;

import com.final_project.final_project.DTO.PriceHistory;
import com.final_project.final_project.models.*;
import com.final_project.final_project.service.PriceHistoryService;
import com.final_project.final_project.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/room")
@CrossOrigin
public class RoomController {

    @Autowired
    private RoomService roomService;
    @Autowired
    private PriceHistoryService priceHistoryService;

    @GetMapping(value = "/number_of_persons/{numberOfPersons}" ,produces = "application/json")
    public List<Room> getRoomByPersons(@PathVariable("numberOfPersons") int number) {
        return roomService.findByNumberOfPersons(number);

    }
    @GetMapping(value = "/price/{price}" ,produces = "application/json")
    public List<Room> getRoomByPrice(@PathVariable("price") double price) {
        return roomService.findByPrice(price);

    }

    @GetMapping(value = "/price_history/{location}")
    public List<PriceHistory> computePriceHistory(@PathVariable("location") String locationName){
        return priceHistoryService.priceHistory(locationName);
    }
    @PutMapping(value = "/room",produces = "application/json")
    public void putRoom(@RequestBody Room room){
       roomService.saveRoom(room);
    }

    @DeleteMapping(value = "/{id}",produces = "application/json")
    public boolean deleteRoom(@PathVariable("id") int id){
       return roomService.deleteRoom(id);
    }

}
