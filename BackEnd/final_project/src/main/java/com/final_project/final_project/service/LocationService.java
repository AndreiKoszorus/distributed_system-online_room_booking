package com.final_project.final_project.service;


import com.final_project.final_project.models.Location;
import com.final_project.final_project.repository.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationService {
    @Autowired
    private LocationRepository locationRepository;

    public double distance(double lat2,double lon2,Location location){
        final int R = 6371; // Radius of the earth
        double lat1 = location.getLatitude();
        double lon1 = location.getLongitude();

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c;


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }
}
