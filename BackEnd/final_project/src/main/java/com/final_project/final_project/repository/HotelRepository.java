package com.final_project.final_project.repository;

import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface HotelRepository extends JpaRepository<Hotel,Integer> {

     Hotel findById(int id);
     List<Hotel> findAllByLocationId(Location location);
     List<Hotel> findAllByStars(int stars);
     List<Hotel> findAllByRating(String rating);
     List<Hotel> findAllByRatingAndStars(String rating,int stars);
}
