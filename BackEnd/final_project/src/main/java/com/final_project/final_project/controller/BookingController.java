package com.final_project.final_project.controller;


import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Location;
import com.final_project.final_project.models.Reservation;
import com.final_project.final_project.models.Room;
import com.final_project.final_project.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/booking")
@CrossOrigin
public class BookingController {

@Autowired
    private BookingService bookingService;
@Autowired
    private ReservationService reservationService;
@Autowired
    private EmailService emailService;

@GetMapping(value = "/{locationName}", produces = "application/json")
    public List<Hotel> getHotelsByLocation(@PathVariable("locationName") String locationName){


        return bookingService.getHotelsByLocation(locationName);

}

    @GetMapping(value = "/details/{checkIn}/{checkOut}/{numberOfPersons}/{hotelName}/{hotelLocation}", produces = "application/json")
    public List<Room> getAvailableRooms(@PathVariable("checkIn") String checkIn,@PathVariable("checkOut") String checkOut,@PathVariable("numberOfPersons") int numberOfPersons,@PathVariable("hotelName") String hotelName,@PathVariable("hotelLocation") String hotelLocation) throws ParseException {


        return bookingService.getAvailableRooms(checkIn,checkOut,numberOfPersons,hotelName,hotelLocation);
}
    @GetMapping(value = "/get_reservations/{username}",produces = "application/json")
        public List<Reservation> getReservation(@PathVariable("username") String username){
            return reservationService.getReservations(username);
    }

    @PutMapping(value = "/reserve",produces = "application/json")
    public void putRoom(@RequestBody Reservation reservation){

        reservationService.addReservation(reservation);
        emailService.sendEmail(reservation.getUsername(),"Reservation",reservation.toString());
    }

    @DeleteMapping(value = "/{id}",produces = "application/json")
    public boolean deleteReservation(@PathVariable("id") int reservationId) throws Exception{
        return bookingService.deleteReservation(reservationId);

}







}
