package com.final_project.final_project.service;

import com.final_project.final_project.models.Reservation;
import com.final_project.final_project.models.User;
import com.final_project.final_project.repository.ReservationRepository;
import com.final_project.final_project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private UserRepository userRepository;


    public void addReservation(Reservation reservation){
        reservationRepository.save(reservation);
    }

    public List<Reservation> getReservations(String username){

        return reservationRepository.findAllByUsername(username);
    }

}
