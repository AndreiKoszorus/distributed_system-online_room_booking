package com.final_project.final_project.controller;

import com.final_project.final_project.models.User;
import com.final_project.final_project.repository.UserRepository;
import com.final_project.final_project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/{username}", produces = "application/json")
    public User getUser(@PathVariable("username") String username) {
        return userService.findUser(username);
    }

    @PutMapping(value = "/user", produces = "application/json")
    public void putUser(@RequestBody User user) {
        userService.saveUser(user);
    }


    @DeleteMapping(value = "/{username}", produces = "application/json")
    public void deleteUser(@PathVariable("username") String username) {
        User user = userService.findUser(username);
        userService.deleteUser(user);
    }

}
