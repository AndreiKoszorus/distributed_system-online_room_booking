package com.final_project.final_project.models;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rooms")
public class Room implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "number_of_persons")
    private int numberOfPersons;

    @Column
    private double price;

    @Column
    private String facilities;

    @Column(name = "room_type")
    private String roomType;

    @Column(name = "room_number")
    private int roomNumber;

    @ManyToOne(targetEntity = Hotel.class)
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    @Column(name = "room_image")
    private String roomImage;

    public Room(int numberOfPersons, double price, String facilities,Hotel hotel,String roomType,int roomNumber,String roomImage) {
        this.numberOfPersons = numberOfPersons;
        this.price = price;
        this.facilities = facilities;
        this.hotel = hotel;
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.roomImage = roomImage;

    }

    public Room(){

    }

    public String getRoomImage() {
        return roomImage;
    }

    public void setRoomImage(String roomImage) {
        this.roomImage = roomImage;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    @Override
    public String toString() {
        return "Room{" +
                ", numberOfPersons=" + numberOfPersons +
                ", price=" + price +
                ", facilities='" + facilities + '\'' +
                '}'+'\n';
    }
}
