package com.final_project.final_project.repository;

import com.final_project.final_project.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocationRepository extends JpaRepository<Location,Integer>{

    List<Location> findAllByLocationName(String locationName);
}
