package com.final_project.final_project.service;

import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Location;
import com.final_project.final_project.models.Reservation;
import com.final_project.final_project.models.Room;
import com.final_project.final_project.repository.HotelRepository;
import com.final_project.final_project.repository.LocationRepository;
import com.final_project.final_project.repository.ReservationRepository;
import com.final_project.final_project.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BookingService {

    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private RoomRepository roomRepository;

    public List<Hotel> getHotelsByLocation(String locationName){
        List<Location> locations = locationRepository.findAllByLocationName(locationName);
        List<Hotel> hotels = new ArrayList<>();
        if(locations!=null && locations.size()!=0) {
            for (Location location : locations) {
                List<Hotel> temporaryHotels = hotelRepository.findAllByLocationId(location);
                hotels.addAll(temporaryHotels);
            }
        }

        return hotels;

    }

    public List<Room> getAvailableRooms(String checkIn,String checkOut,int numberOfPersons, String hotelName,String hotelLocation) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parseCheckIn = simpleDateFormat.parse(checkIn);
        Date parseCheckOut = simpleDateFormat.parse(checkOut);

        Timestamp checkInTime = new Timestamp(parseCheckIn.getTime());
        Timestamp checkOutTime = new Timestamp(parseCheckOut.getTime());

        List<Reservation> reservations = reservationRepository.findAll();
        List<Room> availableRooms = roomRepository.findAll();
        List<Room> availableRoomsPersons = new ArrayList<>();

        System.out.println(checkInTime);

        for(Reservation reservation:reservations){
            if((checkInTime.after(reservation.getCheckIn()) && checkInTime.before(reservation.getCheckOut()) ) || (checkOutTime.after(reservation.getCheckIn()) && checkOutTime.before(reservation.getCheckOut())) || (checkInTime.before(reservation.getCheckIn()) && checkOutTime.after(reservation.getCheckOut()))){

                availableRooms.remove(reservation.getReservedRoom());

            }
        }

        for(Room room:availableRooms){
            if(room.getNumberOfPersons() == numberOfPersons && room.getHotel().getName().equals(hotelName) && room.getHotel().getLocationId().getLocationName().equals(hotelLocation)){
                availableRoomsPersons.add(room);
            }
        }

        return availableRoomsPersons;
    }

    public boolean deleteReservation( int reservationId) throws Exception{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Reservation reservation = reservationRepository.findById(reservationId);

        Date currentTimeParse = simpleDateFormat.parse(simpleDateFormat.format(System.currentTimeMillis()));
        Timestamp currentTime = new Timestamp(currentTimeParse.getTime());
        if(reservation!=null) {
            if ((reservation.getCheckIn().getTime() - currentTime.getTime()) / (60 * 60000) >= 48) {
                reservationRepository.delete(reservation);
                return true;
            }
        }

        return false;

    }
}
