package com.final_project.final_project.controller;

import com.final_project.final_project.models.User;
import com.final_project.final_project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/login")
public class LoginController {


    @Autowired
    private UserService userService;

    @CrossOrigin
    @PostMapping (value = "/credentials",produces = "application/json")
    public User loginUser(@RequestBody User user){
       return userService.findUserByUsernameAndPassword(user.getUsername(),user.getPassword());

    }
    @CrossOrigin
    @PutMapping(value = "/register", produces = "application/json")
    public void putUser(@RequestBody User user) {
        userService.saveUser(user);
    }


}
