package com.final_project.final_project.repository;

import com.final_project.final_project.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUsername(String username);
    User findByUsernameAndAndPassword(String username,String password);
}
