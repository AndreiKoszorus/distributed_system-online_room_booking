package com.final_project.final_project.service;

import com.final_project.final_project.DTO.HotelWithDistance;
import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Location;
import com.final_project.final_project.models.Room;
import com.final_project.final_project.repository.HotelRepository;
import com.final_project.final_project.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HotelService {

    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private LocationService locationService;
    @Autowired
    private RoomRepository roomRepository;


    public Hotel findHotel(int id){
        return hotelRepository.findById(id);
    }
    public void saveHotel(Hotel hotel){
        hotelRepository.save(hotel);
    }

    public boolean deleteHotel(int id){

        Hotel hotel = hotelRepository.findById(id);
        if(hotel!=null){
            List<Room> rooms = roomRepository.findAllByHotel(hotel);
            for(Room room:rooms){
                roomRepository.delete(room);
            }
            hotelRepository.delete(hotel);
            return true;
        }
        return false;
    }

    public List<Hotel> findAllByStars(int stars){
        return hotelRepository.findAllByStars(stars);
    }

    public List<Hotel> findAllByRating(String rating){
        return hotelRepository.findAllByRating(rating);
    }

    public List<Hotel> findAllByRatingAndStars(String rating,int stars){
        return hotelRepository.findAllByRatingAndStars(rating,stars);
    }

    public List<HotelWithDistance> filterByDistance(double distance, double lat1, double lon1){
       List<Hotel> hotels = hotelRepository.findAll();
        List<HotelWithDistance> filteredHotels = new ArrayList<>();

        for(Hotel hotel:hotels){
            double distanceToHotel = locationService.distance(lat1,lon1,hotel.getLocationId());

            if(distanceToHotel<= distance){
                HotelWithDistance hotelWithDistance = new HotelWithDistance(hotel,distanceToHotel);
                filteredHotels.add(hotelWithDistance);
            }
        }

        return filteredHotels;

    }
}
