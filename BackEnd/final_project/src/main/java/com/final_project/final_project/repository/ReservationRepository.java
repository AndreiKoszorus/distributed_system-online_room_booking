package com.final_project.final_project.repository;

import com.final_project.final_project.models.Reservation;
import com.final_project.final_project.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation,Integer> {

    Reservation findById(int id);
    List<Reservation> findAllByUsername(String username);

}
