package com.final_project.final_project.DTO;


import com.final_project.final_project.models.Hotel;

import javax.persistence.*;
import java.io.Serializable;

public class PriceHistory implements Serializable {

    private double averagePrice;


    private Hotel hotel;

    public PriceHistory(double averagePrice, Hotel hotel) {
        this.averagePrice = averagePrice;
        this.hotel = hotel;
    }


    public double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        this.averagePrice = averagePrice;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    @Override
    public String toString() {
        return "PriceHistory{" +
                ", averagePrice=" + averagePrice +
                ", hotel=" + hotel +
                '}';
    }
}
