package com.final_project.final_project.service;

import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Location;
import com.final_project.final_project.DTO.PriceHistory;
import com.final_project.final_project.models.Room;
import com.final_project.final_project.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PriceHistoryService {
    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private RoomRepository roomRepository;

    public List<PriceHistory> priceHistory(String locationName){
        List<Location> locations = locationRepository.findAllByLocationName(locationName);
        List<Hotel> hotels = new ArrayList<>();
        List<PriceHistory> priceHistories = new ArrayList<>();
        if(locations!=null && locations.size()!=0) {
            for (Location location : locations) {
                List<Hotel> temporaryHotels = hotelRepository.findAllByLocationId(location);
                hotels.addAll(temporaryHotels);
            }
            for(Hotel hotel:hotels){
                double averagePrice = 0;
                List<Room> rooms = roomRepository.findAllByHotel(hotel);
                if(rooms!=null && rooms.size()!=0) {
                    for (Room room : rooms) {
                        averagePrice += room.getPrice();
                    }
                    averagePrice = averagePrice/rooms.size();
                    PriceHistory priceHistory = new PriceHistory(averagePrice,hotel);
                    priceHistories.add(priceHistory);
                }
            }
        }

        return priceHistories;
    }


}
