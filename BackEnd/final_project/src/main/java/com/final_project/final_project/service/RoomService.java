package com.final_project.final_project.service;

import com.final_project.final_project.models.Hotel;
import com.final_project.final_project.models.Room;
import com.final_project.final_project.repository.HotelRepository;
import com.final_project.final_project.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class RoomService {

    @Autowired
    RoomRepository roomRepository;


    public void saveRoom(Room room){
        roomRepository.save(room);
    }

    public Room findRoom(int id){
        return roomRepository.findById(id);
    }

    public List<Room> getAllRooms(){
        return roomRepository.findAll();
    }

    public boolean deleteRoom(int  id){
        Room room = roomRepository.findById(id);
        if(room!=null){
            roomRepository.delete(room);
            return true;
        }

        return false;

    }

    public List<Room> findByPrice(double price){
        return roomRepository.findAllByPrice(price);
    }

    public List<Room> findByNumberOfPersons(int numberOfPerson){
        return roomRepository.findAllByNumberOfPersons(numberOfPerson);
    }
}
