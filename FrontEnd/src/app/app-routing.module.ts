import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {HomeComponent} from './pages/home/home.component';
import {HotelComponent} from './pages/hotel/hotel.component';
import {ReservationsComponent} from './pages/reservations/reservations.component';
import {AuthGuard} from './pages/auth-guard.service';
import {ChartComponent} from './pages/chart/chart.component';

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'hotel/:id', component: HotelComponent, canActivate: [AuthGuard]},
  {path: 'reservations', component: ReservationsComponent, canActivate: [AuthGuard]},
  {path:'charts',component:ChartComponent,canActivate:[AuthGuard]},
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
