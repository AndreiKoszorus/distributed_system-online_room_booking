import {Component, OnInit} from '@angular/core';
import {HomeService} from './home.service';
import {NavigationExtras, Router} from '@angular/router';
import {SearchCacheService} from '../../search-cache.service';
import {DatePipe} from '@angular/common';
import {HotelService} from '../hotel/hotel.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  selectedTitle;
  filter;
  numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  hotels = [];
  cachedHotels = [];
  filters;
  location;
  checkInDate;
  checkOutDate;
  numberOfPersons;
  hotelWithDistance = true;
  latitude;
  longitude;
  showPriceHistory;

  constructor(private homeService: HomeService, private router: Router, private cacheService: SearchCacheService, public datePipe: DatePipe,private snackBar:MatSnackBar) {
  }


  ngOnInit() {
    this.showPriceHistory = false;
    this.filters = [
      {
        name: 'Rating',
        options: ['Good', 'Medium', 'Bad'],
      },
      {
        name: 'Stars',
        options: ['Ascending', 'Descending'],
      },

      {
        name: 'Location in km',
        options: ['0.5', '1','2','3','4','5','10'],
      },

    ];
    navigator.geolocation.getCurrentPosition(res=>{
      this.latitude = res.coords.latitude;
      this.longitude = res.coords.longitude;
    });
  }

  returnByFilter(filter, option) {
    if (filter.name === 'Rating') {
      return this.filterByRating(option);
    }
    else if (filter.name === 'Stars') {
      console.log('enter');
      return this.filterByPrice(option);
    }
    else{
      return this.filterByLocation(option);
    }

  }

  filterByRating(rating) {
    console.log(this.hotels);
    console.log(rating);
    this.hotelWithDistance = true;
    this.hotels = this.cachedHotels;
    this.hotels = this.hotels.filter(hotel => {
      return hotel.rating === rating;
    });

    console.log(this.hotels);
  }

  filterByPrice(hotels) {
    this.hotelWithDistance = true;
    this.hotels = this.cachedHotels;
    this.hotels = this.hotels.sort(function (a, b) {
      return hotels === 'Ascending' ? a.stars - b.stars : b.stars - a.stars;
    });
    console.log('sorted', this.hotels);
  }

  filterByLocation(distance){
    this.homeService.filterHotelByDistance(distance,this.latitude,this.longitude).subscribe(response=>{
      if(response){
        this.hotelWithDistance = false;
        this.hotels = response;
        this.hotels.map(hotel => {
          console.log(hotel.distance)
         let obj = {starsArray: []};
          for (let i = 0; i < hotel.hotel.stars; i++) {
            obj.starsArray.push('');
          }
          Object.assign(hotel, obj);
        });
      }
    })
  }

  search() {
    this.hotelWithDistance = true;

    if (this.numberOfPersons && this.checkOutDate && this.checkInDate && this.location) {

      this.homeService.search(this.location).subscribe(response => {
        if (response) {
          if(response.length!=0) {
            this.showPriceHistory = true;
          }
          else {
            this.openSnackBar('No hotels at this location','');
          }
          this.cachedHotels = response;
          this.hotels = response;
          let obj = {starsArray: []};
          this.hotels.map(hotel => {
            obj = {starsArray: []};
            for (let i = 0; i < hotel.stars; i++) {
              obj.starsArray.push('');
            }
            Object.assign(hotel, obj);
          });
        }
      });
    }
    else{
      this.openSnackBar('Please fill all the fields','');
    }
  }

  navigateToHotel(hotel: any) {
    this.router.navigate(['/hotel', hotel.name]);
    this.cacheService.cacheSearchedItems(this.datePipe.transform(this.checkInDate, 'yyyy-MM-dd'), this.datePipe.transform(this.checkOutDate, 'yyyy-MM-dd'), this.numberOfPersons, this.location, hotel);

  }

  showHistory() {
    let navigationExtras: NavigationExtras ={
      queryParams:{
        "location":this.location
      }
    }
    this.router.navigate(['charts'],navigationExtras);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });
  }
}
