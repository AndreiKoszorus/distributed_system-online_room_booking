import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  bookingURL= 'http://localhost:8080/booking';
  hotelsURL = 'http://localhost:8080/hotels/filter_by_distance';

  constructor(private http: HttpClient) { }

  search(location): Observable<any> {

    return this.http.get(this.bookingURL + '/' + location.toLowerCase());


  }

  filterHotelByDistance(distance,latitude,longitude): Observable<any>{
    console.log(latitude);
    return this.http.get(this.hotelsURL + '/'+latitude+'/'+longitude+'/'+distance);

  }




}
