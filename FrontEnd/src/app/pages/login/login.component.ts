import {Component, OnInit} from '@angular/core';
import {LoginService} from './login.service';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {CachedUsernameService} from '../../cached-username.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private username: string;
  private password: string;
  mockUsername = 'koszorus.andrei@gmail.com';
  mockPassword = 'andrei123';

  constructor(private loginService: LoginService,
              private router: Router, private  snackBar: MatSnackBar,private chachedUsername:CachedUsernameService) {

  }

  ngOnInit() {
    this.username = this.mockUsername;
    this.password = this.mockPassword;
  }

  login() {
    if (this.username && this.password) {
      this.loginService.login(this.username, this.password)
        .subscribe(response => {
          if (response) {
            this.chachedUsername.cacheUsername(this.username);
            sessionStorage.setItem('Auth', 'authenticated');
            this.router.navigate(['/home']);
            this.openSnackBar('Login successful', '');

          } else {
            this.openSnackBar('Wrong username or password', '');
          }
        });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });
  }
}

