import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
loginUrl = 'http://localhost:8080/booking/details';

  constructor(private http: HttpClient) { }

  search(search): Observable<any> {
    return this.http.get(this.loginUrl + '/' + search.checkIn + '/' + search.checkOut + '/' + search.numberOfPersons + '/' + search.hotelName + '/' + search.hotelLocation);

  }
}
