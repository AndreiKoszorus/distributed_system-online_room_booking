import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  reservationURL = 'http://localhost:8080/booking/reserve';
  getReservationsURL = 'http://localhost:8080/booking/get_reservations';
  deleteReservationURL = 'http://localhost:8080/booking';
  constructor(private http:HttpClient) { }

  reserve(reservation):Observable<any>{
    return this.http.put(this.reservationURL,reservation);
  }

  getReservations(username):Observable<any>{
    return this.http.get(this.getReservationsURL + '/' + username);
  }

  deleteReservation(reservationId):Observable<any>{
    return this.http.delete(this.deleteReservationURL +'/'+reservationId);
  }


}
