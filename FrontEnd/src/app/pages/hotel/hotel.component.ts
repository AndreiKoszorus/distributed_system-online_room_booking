import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivateRoutes} from '@angular/router/src/operators/activate_routes';
import {SearchCacheService} from '../../search-cache.service';
import {HotelService} from './hotel.service';
import {DatePipe} from '@angular/common';
import {ReservationService} from './reservation.service';
import {MatSnackBar} from '@angular/material';
import {CachedUsernameService} from '../../cached-username.service';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  private search = {};
  rooms = [];
  reservation = {};

  constructor(private activatedRoute: ActivatedRoute, private cacheService: SearchCacheService, private hotelService: HotelService,private reservationService:ReservationService,private  snackBar: MatSnackBar,private cachedUsername:CachedUsernameService) {
  }



  ngOnInit() {
    // this.cacheService.cacheSearchedItems(this.datePipe.transform('Wed Mar 23 2019 00:00:00 GMT+0200 (Eastern European Standard Time)','yyyy-MM-dd'),this.datePipe.transform('Thu Mar 25 2019 00:00:00 GMT+0200 (Eastern European Standard Time)','yyyy-MM-dd'), 5, 'cluj napoca');
    this.search = Object.assign(this.cacheService.getCachedData(), {hotelName: this.activatedRoute.snapshot.params.id});
    this.search["hotelName"] = this.search["hotelName"].charAt(0).toUpperCase() + this.search["hotelName"].slice(1);
    console.log(this.search);
    this.hotelService.search(this.search).subscribe(response => {
      if (response) {
        this.rooms = response;
        if(this.rooms.length == 0){
          this.openSnackBar("No available rooms to show",'');
        }
        console.log(response);
      }
    });
  }

  reserve(room) {

    this.reservation = {
      checkIn:this.search["checkIn"],
      checkOut:this.search["checkOut"],
      hotel:this.search["hotel"],
      reservedRoom:room,
      username:this.cachedUsername.getUsername()

    };
    this.reservationService.reserve(this.reservation).subscribe(() =>{

      this.openSnackBar("Reservation added check your email",'');

    });



  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });
  }
}
