import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChartService {

  chartURL = 'http://localhost:8080/room/price_history'

  constructor(private http:HttpClient) { }

  getChartData(location):Observable<any>{
    return this.http.get(this.chartURL + '/' + location);

  }



}
