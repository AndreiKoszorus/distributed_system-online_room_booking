import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChartService} from './chart.service';
import {Chart} from 'chart.js'

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit,AfterViewInit {



  location;
  chartData = [];
  chartLables = [];
  chart: any;
  @ViewChild('myChart') myChart;
  ngAfterViewInit(): void {
    this.initCanvas();
  }

  constructor(private route: ActivatedRoute, private chartService: ChartService) {
    this.route.queryParams.subscribe(param => {
      this.location = param["location"];
    });
  }

  ngOnInit() {

  }

  initCanvas() {
    this.chartService.getChartData(this.location).subscribe(respond => {
      if (respond) {
        console.log(respond);

        for (const tuples of respond) {
          this.chartData.push(tuples.averagePrice);
          this.chartLables.push(tuples.hotel.name);
        }
        this.chart = new Chart(this.myChart.nativeElement.getContext('2d'), {
          type: 'bar',
          data: {
            labels: this.chartLables,
            datasets: [
              {
                data: this.chartData,
                borderColor: '#3cba9f',
                fill: false
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {
              xAxes: [{
                display: true
              }],
              yAxes: [{
                display: true
              }],
            }
          }
        });
      }
    });
  }


}







