import { Component, OnInit } from '@angular/core';
import {ReservationService} from '../hotel/reservation.service';
import {CachedUsernameService} from '../../cached-username.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit {

  reservations = [];

  constructor(private reservationService: ReservationService, private chachedUsername: CachedUsernameService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.reservationService.getReservations(this.chachedUsername.getUsername()).subscribe(response => {
      if (response) {
        this.reservations = response;
      }
    })
  }

  removeReservation(reservation, i: any) {
    this.reservationService.deleteReservation(reservation.id).subscribe(response => {
      if (response) {
        this.openSnackBar("Reservation was deleted",'');
        this.reservations.splice(i, 1);
      }
      else{
        this.openSnackBar("You cant't delete this reservation anymore.There are less than 48 hours",'');
      }
    })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      direction: 'ltr',
      horizontalPosition: 'end',
      verticalPosition: 'bottom'
    });
  }
}
