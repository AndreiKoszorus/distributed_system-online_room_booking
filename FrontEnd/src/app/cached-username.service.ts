import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CachedUsernameService {

  private username:string;

  constructor() {

  }
  cacheUsername(username){
    this.username = username;
  }
  getUsername(){
    return this.username;
  }
}
