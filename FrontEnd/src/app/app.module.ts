import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './pages/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule, MatNativeDateModule, MatSelectModule,
  MatSnackBarModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {LoginService} from './pages/login/login.service';
import {HomeComponent} from './pages/home/home.component';
import {CommonModule, DatePipe} from '@angular/common';
import {HotelComponent} from './pages/hotel/hotel.component';
import {SearchCacheService} from './search-cache.service';
import {HotelService} from './pages/hotel/hotel.service';
import {ReservationService} from './pages/hotel/reservation.service';
import {CachedUsernameService} from './cached-username.service';
import { ReservationsComponent } from './pages/reservations/reservations.component';
import { ChartComponent } from './pages/chart/chart.component';
import {ChartService} from './pages/chart/chart.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HotelComponent,
    ReservationsComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatIconModule,
    CommonModule
  ],
  providers: [LoginService, SearchCacheService,HotelService,DatePipe,ReservationService,CachedUsernameService,ChartService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
