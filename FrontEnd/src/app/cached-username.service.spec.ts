import { TestBed } from '@angular/core/testing';

import { CachedUsernameService } from './cached-username.service';

describe('CachedUsernameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CachedUsernameService = TestBed.get(CachedUsernameService);
    expect(service).toBeTruthy();
  });
});
