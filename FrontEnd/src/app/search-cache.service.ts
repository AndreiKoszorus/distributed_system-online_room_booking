import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchCacheService {
  private cachedData = { };

  constructor() { }

  cacheSearchedItems(checkIn, checkOut, numberOfPersons, hotelLocation,hotel) {
    this.cachedData = Object.assign({checkIn, checkOut, numberOfPersons, hotelLocation,hotel});
    console.log(this.cachedData);
  }

  getCachedData() {
    return this.cachedData;
  }
}
